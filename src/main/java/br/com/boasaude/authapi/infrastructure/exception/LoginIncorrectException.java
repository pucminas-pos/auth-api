package br.com.boasaude.authapi.infrastructure.exception;

public class LoginIncorrectException extends RuntimeException {

    public LoginIncorrectException() {
    }

    public LoginIncorrectException(String message) {
        super(message);
    }

    public LoginIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoginIncorrectException(Throwable cause) {
        super(cause);
    }
}
