package br.com.boasaude.authapi.infrastructure.api;

import br.com.boasaude.authapi.domain.model.external.integrationservice.User;
import br.com.boasaude.authapi.domain.model.external.portal.LoginRequest;
import br.com.boasaude.authapi.domain.model.internal.ExceptionMessage;
import br.com.boasaude.authapi.infrastructure.api.pool.IntegrationServicePoolConfig;
import br.com.boasaude.authapi.infrastructure.exception.IntegrationException;
import br.com.boasaude.authapi.infrastructure.exception.IntegrationServiceException;
import br.com.boasaude.authapi.infrastructure.exception.LoginIncorrectException;
import com.google.gson.Gson;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.Reader;
import java.nio.charset.StandardCharsets;

@FeignClient(value = "integrationService-api", url = "${application.integration-service.url}",
        configuration = {IntegrationServiceAPI.IntegrationServiceErrorDecoder.class, IntegrationServicePoolConfig.class})
public interface IntegrationServiceAPI {

    @PostMapping("users/login")
    User login(@RequestBody LoginRequest loginRequest);


    class IntegrationServiceErrorDecoder implements ErrorDecoder {

        @Autowired
        public Gson gson;

        @Override
        public Exception decode(String method, Response response) {
            final HttpStatus status = HttpStatus.valueOf(response.status());

            if (status == HttpStatus.INTERNAL_SERVER_ERROR)
                return new IntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            try {
                Reader reader = response.body().asReader(StandardCharsets.UTF_8);
                ExceptionMessage exceptionMessage = gson.fromJson(reader, ExceptionMessage.class);
                return new IntegrationException(exceptionMessage.getMessage(), status);

            } catch (Exception ex) {
                return new IntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}
