package br.com.boasaude.authapi.domain.model.external.portal;

import br.com.boasaude.authapi.domain.model.external.integrationservice.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {

    private String email;
    private String password;
    private UserRole role;
}
