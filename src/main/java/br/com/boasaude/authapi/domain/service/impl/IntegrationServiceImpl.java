package br.com.boasaude.authapi.domain.service.impl;

import br.com.boasaude.authapi.domain.model.external.integrationservice.User;
import br.com.boasaude.authapi.domain.model.external.integrationservice.UserRole;
import br.com.boasaude.authapi.domain.model.external.portal.LoginRequest;
import br.com.boasaude.authapi.domain.service.IntegrationService;
import br.com.boasaude.authapi.infrastructure.api.IntegrationServiceAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class IntegrationServiceImpl implements IntegrationService {

    private final IntegrationServiceAPI integrationServiceAPI;

    @Override
    public User login(LoginRequest loginRequest) {
        return integrationServiceAPI.login(loginRequest);
    }
}
