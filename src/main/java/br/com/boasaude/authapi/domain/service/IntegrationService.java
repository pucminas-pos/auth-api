package br.com.boasaude.authapi.domain.service;

import br.com.boasaude.authapi.domain.model.external.integrationservice.User;
import br.com.boasaude.authapi.domain.model.external.portal.LoginRequest;

public interface IntegrationService {

    User login(LoginRequest loginRequest);
}
