package br.com.boasaude.authapi.domain.service.impl;

import br.com.boasaude.authapi.domain.model.external.integrationservice.User;
import br.com.boasaude.authapi.domain.model.external.integrationservice.UserRole;
import br.com.boasaude.authapi.domain.model.external.portal.LoginRequest;
import br.com.boasaude.authapi.domain.model.external.portal.LoginResponse;
import br.com.boasaude.authapi.domain.service.AuthService;
import br.com.boasaude.authapi.domain.service.IntegrationService;
import br.com.boasaude.authapi.infrastructure.exception.TokenExpiredException;
import br.com.boasaude.authapi.infrastructure.util.DateUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private static final String TOKEN_PREFIX = "Bearer";

    @Value("${application.jwt.secret}")
    private String jwtSecret;

    @Value("${application.jwt.expiration.minute:30}")
    private Integer jwtExpiration;

    @Value("${application.jwt.issuer}")
    private String jwtIssuer;

    private final IntegrationService integrationService;


    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        User user = integrationService.login(loginRequest);
        String token = generateToken(user);
        return LoginResponse.builder().token(token).date(LocalDateTime.now().toString()).build();
    }

    @Override
    public User validateToken(String authToken) {
        try {
            DefaultClaims claims = (DefaultClaims) Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parse(StringUtils.replaceIgnoreCase(authToken, TOKEN_PREFIX, ""))
                    .getBody();

            return User.builder().email(claims.getSubject()).role(UserRole.valueOf(claims.get("role").toString())).build();

        } catch (ExpiredJwtException ex) {
            throw new TokenExpiredException("token expired");
        }
    }

    public String generateToken(User user) {
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setSubject(user.getEmail())
                .setId(UUID.randomUUID().toString())
                .setIssuer(jwtIssuer)
                .setIssuedAt(new Date())
                .setExpiration(DateUtil.convert(LocalDateTime.now().plusMinutes(jwtExpiration)))
                .claim("role", user.getRole().toString())
                .signWith(SignatureAlgorithm.HS256, jwtSecret)
                .compact();
    }
}
