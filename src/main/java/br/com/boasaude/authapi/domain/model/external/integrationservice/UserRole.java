package br.com.boasaude.authapi.domain.model.external.integrationservice;

public enum UserRole {

    ASSOCIATE, ADMIN

}
