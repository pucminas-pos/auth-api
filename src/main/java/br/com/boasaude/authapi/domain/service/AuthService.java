package br.com.boasaude.authapi.domain.service;

import br.com.boasaude.authapi.domain.model.external.integrationservice.User;
import br.com.boasaude.authapi.domain.model.external.portal.LoginRequest;
import br.com.boasaude.authapi.domain.model.external.portal.LoginResponse;

public interface AuthService {

    LoginResponse login(LoginRequest loginRequest);

    User validateToken(String authToken);
}
