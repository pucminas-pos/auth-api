package br.com.boasaude.authapi.application.controller;

import br.com.boasaude.authapi.domain.model.external.integrationservice.User;
import br.com.boasaude.authapi.domain.model.external.portal.LoginRequest;
import br.com.boasaude.authapi.domain.model.external.portal.LoginResponse;
import br.com.boasaude.authapi.domain.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("authentication")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;


    @PostMapping("login")
    public ResponseEntity login(@RequestBody LoginRequest loginRequest) {
        LoginResponse loginResponse = authService.login(loginRequest);
        return ResponseEntity.ok(loginResponse);
    }

    @PostMapping("token")
    public ResponseEntity validateToken(@RequestHeader("Authorization") String authToken) {
        User user = authService.validateToken(authToken);
        return ResponseEntity.ok(user);
    }
}
