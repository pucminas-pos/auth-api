package br.com.boasaude.authapi.application.controller;

import br.com.boasaude.authapi.infrastructure.exception.IntegrationException;
import br.com.boasaude.authapi.infrastructure.exception.LoginIncorrectException;
import br.com.boasaude.authapi.infrastructure.exception.TokenExpiredException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler(Exception.class)
    public ResponseEntity exception(Exception ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(createErrorMap("unexpected error"));
    }

    @ExceptionHandler(LoginIncorrectException.class)
    public ResponseEntity loginIncorrectException(LoginIncorrectException ex) {
        log.error("loginIncorrectException: ", ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(TokenExpiredException.class)
    public ResponseEntity tokenExpiredException(TokenExpiredException ex) {
        log.error("tokenExpiredException: ", ex);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(createErrorMap(ex.getMessage()));
    }

    @ExceptionHandler(IntegrationException.class)
    public ResponseEntity integrationException(IntegrationException ex) {
        log.error("error: ", ex);
        return ResponseEntity.status(ex.getHttpStatus()).body(createErrorMap(ex.getMessage()));
    }

    private final Map createErrorMap(String message) {
        Map<String, String> map = new HashMap<>();
        map.put("message", message);
        map.put("date", LocalDateTime.now().toString());
        return map;
    }
}
