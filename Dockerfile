FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/auth-api/src
COPY pom.xml /home/auth-api
RUN mvn -f /home/auth-api/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/auth-api/target/auth-api.jar /usr/local/lib/auth-api.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","/usr/local/lib/auth-api.jar"]